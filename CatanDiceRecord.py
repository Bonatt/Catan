import ROOT
import numpy as np
from random import randint
import os.path


print ''
# Redefine quit to something shorter
def ex():
  quit()

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(0.85, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();





# Catan Dice-roll Distribution
# I most likely missed (more than) a few rolls here.
# To do:
#   1. Compare this set with normal distributions.
#   2. Compare rolls of Josh, Nick, and Mark, e.g.. Do we all roll normal distributions?
#   3. If I recoreded red and yellow die distinctly, I could then figure out which (if any) di(c)e account(s) for anomolies.

##########################################################################
# SEE PREVIOUS VERSIONS FOR SNIPPETS OF OTHER INTERESTING CODES NOT HERE #
##########################################################################

# Catan Seafares Scenario 1. 1/27/17, 3:45 PM - 5:30 PM. Josh, Nick (winner), Mark (that turn order). 
Game1 = [5,10,7,11,8,8,7,8,7,3,9,8,8,8,6,6,6,4,9,2,4,4,10,8,8,6,3,9,11,4,10,9,6,2,3,10,10,9,10,6,9,9,11,6,6,8,6,10,7,2,6,8,6,4,8,4,7,5,6,8,11,7,8,5,6,6,8,7,6,8,6,9,9,11,9,9,4,6,11,7,7,12,9,8,9,4,6,7,7,10,6,7,7]

# Catan Seafarers Scenario 3. 1/12/17, 8:30 PM - XX:XX PM. Josh, Nick (winner), Mom (that turn order).
Game2 = [4,3,8,9,4,10,6,5,7,6,9,3,5,10,7,6,7,12,2,11,8,10,4,7,8,6,9,7,4,5,8,12,5,5,9,5,5,6,10,3,10,9,8,5,6,7,7,7,10,5,8,5,10,5,7,6,7,2,12,6,7,2,10,4,4,6,5,11,9,8,7,6,9,11,3,9,7,4,2,9,8,7,6,6,6,7,9,9,7,9,7,11,9,8,6,8,5,7,7,8,6,10,5,9,7,5,9,3,7,7,10,7,8,7,12,6,8,9,10,7,9,10,6,8,8,6]

# Homemade random scenario. 1/29/17, 2:30 PM - 4:00 PM. Mom, Nick, Josh (winner) (that turn order).
Game3 = [10,6,8,9,5,4,7,4,6,7,8,4,3,9,4,7,10,4,5,6,5,5,6,7,5,4,6,10,8,3,3,4,6,4,7,11,7,7,6,5,3,8,8,3,4,8,3,9,5,12,7,7,9,3,6,9,9,12,9,11,5,9,7,6,6,7,9,7,7,7,10,7,11,8,10,8,8,7,7,8,9,7,8,12,6,8,5,8]

# Homemade random scenario. 1/29/17, 4:30 PM - ?:?? PM. Nick (winner), Josh, Mom (that turn order).
Game4 = [5,10,8,6,5,8,6,7,9,7,11,10,7,12,10,7,8,8,6,7,9,3,2,5,5,7,9,8,5,6,6,7,9,5,5,6,6,7,9,7,10,10,8,8,6,6,12,6,5,9,5,6,6,11,8,4,10,9,6,8,8,7,5,11,4,12,5,10,6,5,7,6,9,4,7,7,6,7,6,8,6,5,6,7,6,11,6,5,2,8,6,10,7,7,8,4,7,7,7,5,7,9,2,6,7,7,8,7,6,7,7,8,11,10,6,5,4,7,8,5,3,12,6,7,8,8,4,6,7,10,7,10,8,6,6,9,3,6,2,6,7,4,6,10,9,7,7,7,5,6,6,8,8,7,6,5,11,9,9,6,8,5,10,7,9,9,10,9,4,2,7,7]

# Catan. 2/17/19, 7:45 PM - 10:10 PM. Mom, Josh, Emilie (winner) (that turn order).
Game5 = [8,6,11,11,7,8,11,4,8,9,12,7,3,7,9,5,11,8,8,5,9,3,5,6,6,5,3,7,6,7,12,11,11,8,4,9,12,8,4,9,6,7,3,8,2,5,6,11,12,10,5,11,5,6,7,7,7,7,2,12,12,3,11,8,8,4,3,6,2,10,7,8,6,8,6,10,4,4,7,8,4,11,4,3,4,6,7,5]

# 3/2/17, 9:00 PM - ??:??. Josh, Emilie (winner), Mom (that turn order). Mom quit at index 78
Game6 = [8,7,9,9,8,7,4,6,4,12,7,8,7,8,9,10,7,6,9,5,7,6,4,5,4,5,4,9,9,5,8,5,4,5,9,8,9,12,7,8,9,9,8,8,10,9,3,10,10,7,10,4,11,7,7,8,9,6,8,7,9,9,3,7,9,5,10,3,9,2,5,9,9,5,10,7,4,7,8,6,12,8,7,4,6,8]

# 3/25/17, 9:15 PM - 10:30 PM. Josh (winner), Nick, Monica (that turn order).
Game7 = [8,8,5,12,4,7,6,11,6,7,6,8,6,8,9,8,5,6,11,8,6,8,7,9,11,5,4,11,3,8,7,11,8,5,8,7,12,11,7,7,7,5,4,9,4,7,8,9,7,8,8,8,4,2,9,9,3,9,8,6,9,8,8]

# 12/30/17, 9 PM - 10:30. Emilie, Josh (winner), Nick, Mom (that turn order).
# Forgot to record first few rounds. Not recording now...
#Game8 = [6,]




# Fill individual histograms and save as root files. Add them to aggregate histo THStack.
print 'Filling histograms...'
GameN = [Game1, Game2, Game3, Game4, Game5, Game6, Game7]

# Stack of histos. Seems like it makes things super easy. 
# From https://root.cern.ch/root/html512/THStack.html
hStack = ROOT.THStack('hStack','Characterization of Catan Dice;Sum;Probability')

for N,Game in enumerate(GameN):
  hGame = ROOT.TH1D('hGame'+str(N+1), 'hGame'+str(N+1), 14, 0, 14)
  
  for roll in Game:
    hGame.Fill(roll)

  hGame.Scale(1./len(Game)) # Scale histogram by sample size.
  hGame.GetXaxis().SetNdivisions(221)
  hGame.SetFillColor(N+2)#41+N)#ROOT.kBlue-10+N)#N+2) # Own special color.
  hGame.SetLineColor(N+2)#41+N)#ROOT.kBlue-10+N)#N+2)
  hGame.SetLineWidth(2)
  hGame.SetFillStyle(3003)
  hGame.SetStats(0)

  hStack.Add(hGame)

  ### If file does not already exists, create one. Why not? Maybe we can use it later.  
  if os.path.isfile('Game'+str(N+1)+'.root') == False:
    hGame.SaveAs('Game'+str(N+1)+'.root')




### If file already exists, use it instead of creating (long) below.
if os.path.isfile('Theory.root') == True:
  fileTheory = ROOT.TFile.Open('Theory.root')
  hTheory = fileTheory.Get('hTheory')
  hTheory.SetLineWidth(2)
  hTheory.SetLineStyle(ROOT.kDashed)


nd = int(1e6)
if os.path.isfile('Theory.root') == False:
  # Fill histogram with Theoretical array
  # From http://stackoverflow.com/questions/33069476/simulating-rolling-2-dice-in-python
  print 'Simulating 2d6...'
  def dice(n):
      return [randint(1, 6) + randint(1, 6) for _ in range(n)]
  #nd = int(1e6)
  Theory = dice(nd)
  
  hTheory = ROOT.TH1D('hTheory', 'hTheory', 14, 0, 14)
  for i in Theory:
    hTheory.Fill(i)
  hTheory.Scale(1./len(Theory))
  hTheory.SetFillColor(ROOT.kBlack)
  hTheory.SetLineColor(ROOT.kBlack)
  hTheory.SetLineWidth(2)
  hTheory.SetLineStyle(ROOT.kDashed)
  hTheory.SetFillStyle(3003)
  hTheory.SetStats(0)
  
  hTheory.SaveAs('Theory.root')

#hStack.Add(hTheory)




### Sum hStack and save as new histo
hStackSum = hStack.GetStack().Last()#.Clone()
hStackSum.SetTitle('hStackSum;;')
hStackSum.Scale(1./len(GameN))#hStackSum.GetEntries())
#hStackSum.SetFillColor(ROOT.kBlack)
hStackSum.SetLineColor(ROOT.kBlack)
hStackSum.SetLineWidth(2)
hStackSum.SetFillStyle(0)

#hStackSum.Add(hStackSum)

if os.path.isfile('GameN.root') == False:
  hStackSum.SaveAs('GameN.root')




### Create canvas for all histograms
c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGridy()
hStack.Draw('hist nostack') # No e on these because ~no error on acquisition.
hStack.GetXaxis().SetNdivisions(221)
hTheory.Draw('hist same')
hStackSum.Draw('hist e same')
# The uncertainties are probably the diff between max and min recording, but
# kept stock here. It's not all that important because this is not important.

# Legend
x1 = 0.75
y1 = 0.6
x2 = 0.89
y2 = 0.89
legend = ROOT.TLegend(x1, y1, x2, y2)
legend.AddEntry(hStackSum,  'Experimental, '+str('{:,}'.format(int(hStack.GetStack().Last().GetEntries()))), 'l')

# Fills legend with games
hStackList = hStack.GetHists()
for i in range(N+1):
  legend.AddEntry(hStackList[i], '     Game'+str(i+1)+', '+str('{:,}'.format(int(hStackList[i].GetEntries()))), 'f')

legend.AddEntry(hTheory,  'Theoretical, '+str(int(nd/1e6))+'e6', 'l')
legend.SetFillColor(0)
legend.Draw()

#c.SaveAs('CatanDiceRecord.pdf')
c.SaveAs('CatanDiceRecord.png')










### Fit Theory, StackSum. Find diff?
f = ROOT.TF1('f', 'gaus')
f.SetParameter(0, .16667)
f.SetParameter(1, 1.0)
f.SetParameter(2, 7.)
f.SetLineStyle(ROOT.kDashed)
#fgcolor = ROOT.kGray+2
#f.SetLineColor(fgcolor)
#f.SetLineWidth(1)

#hTheory.Fit('gaus')
#hStackSum.Fit('gaus')


# Theory fit and parameters, and resulting function
hTheory.Fit('f', '0+', '', 0, 14)

fTheoryConstant = f.GetParameter(0)
fTheoryConstantUnc = f.GetParError(0)
fTheoryMean = f.GetParameter(1)
fTheoryMeanUnc = f.GetParError(1)
fTheorySigma = f.GetParameter(2)
fTheorySigmaUnc = f.GetParError(2)

fTheory = ROOT.TF1('fTheory', 'gaus', 0, 14)
fTheory.SetParameter(0, fTheoryConstant)
fTheory.SetParError(0, fTheoryConstantUnc)
fTheory.SetParameter(1, fTheoryMean)
fTheory.SetParError(1, fTheoryMeanUnc)
fTheory.SetParameter(2, fTheorySigma)
fTheory.SetParError(2, fTheorySigmaUnc)
fTheory.SetLineColor(ROOT.kRed)
fTheory.SetLineStyle(ROOT.kDashed)

# StackSum fit and parameters, and resulting function
hStackSum.Fit('f', '0+', '', 0, 14)
fStackSumConstant = f.GetParameter(0)
fStackSumConstantUnc = f.GetParError(0)
fStackSumMean = f.GetParameter(1)
fStackSumMeanUnc = f.GetParError(1)
fStackSumSigma = f.GetParameter(2)
fStackSumSigmaUnc = f.GetParError(2)

fStackSum = ROOT.TF1('fStackSum', 'gaus', 0, 14)
fStackSum.SetParameter(0, fStackSumConstant)
fStackSum.SetParError(0, fStackSumConstantUnc)
fStackSum.SetParameter(1, fStackSumMean)
fStackSum.SetParError(1, fStackSumMeanUnc)
fStackSum.SetParameter(2, fStackSumSigma)
fStackSum.SetParError(2, fStackSumSigmaUnc)
fStackSum.SetLineColor(ROOT.kRed)

### Plot above
c2 = ROOT.TCanvas('c2', 'c2', 1366, 720)
c2.SetGridy()
c2.Divide(1,2)
c2.cd(1)
hStackSum.SetTitle(';Sum;Probability')
hStackSum.Draw('hist e')
hTheory.Draw('hist same')
fTheory.Draw('same')
fStackSum.Draw('same')


### Diff of theory and sum to find probability diff
hDiff = hStackSum-hTheory
hDiff.SetTitle(';Sum;Probability difference')
hDiff.SetLineStyle(9)#ROOT.kSolid)
# Returning to code, line below outputs all zeros instead of some difference.
# Must be difference in ROOT versions. Unsure of previous and current version.
#fDiff = ROOT.TF1('fDiff', 'fStackSum-fTheory', 0, 14)
# for i in range(14): fStackSum(i)-fTheory(i) = correct
# for i in range(14): fDiff(i) = zeros
# Instead, hardcode function...
fDiff = ROOT.TF1('fDiff', '([0]*exp(-0.5*((x-[1])/[2])*((x-[1])/[2]))) - ([3]*exp(-0.5*((x-[4])/[5])*((x-[4])/[5])))', 0, 14)
params = [fStackSumConstant, fStackSumMean, fStackSumSigma, fTheoryConstant, fTheoryMean, fTheorySigma]
for i,p in enumerate(params):
  fDiff.SetParameter(i,p)
fDiff.SetLineColor(ROOT.kRed)
fDiff.SetLineStyle(9)
#c3 = ROOT.TCanvas('c3', 'c3', 1366, 720)
#c3.SetGridy()
c2.cd(2)
hDiff.Draw('hist')
fDiff.Draw('l same')

# Returning to this code, new ROOT version does not get Uymax() properly.
# Only outputs correctly once c is saved. Save, draw, resave.
# Maybe an c2.Update() would work to, but whatever.
c2.SaveAs('Gaussian.png')

# Draw line at y=0 for easier reference.
line = ROOT.TLine(0,0,14,0)
line.SetLineColor(ROOT.kGray+2)
line.SetLineStyle(ROOT.kDashed)
line.SetLineWidth(1)
line.Draw('same')
# Draw line at x=7 for easier reference
line2 = ROOT.TLine(7,ROOT.gPad.GetUymin(),7,ROOT.gPad.GetUymax())
line2.SetLineColor(ROOT.kGray+2)
line2.SetLineStyle(ROOT.kDashed)
line2.SetLineWidth(1)
line2.Draw('same')


c2.SaveAs('Gaussian.png')
#c2.SaveAs('Gaussian.pdf')
