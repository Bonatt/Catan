import ROOT
import numpy as np
from random import randint

print ''
# Redefine quit to something shorter
def ex():
  quit()

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(53);
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();





# Catan Seafarers Dice-roll Distribution
# I most likely missed (more than a) few rolls here.
# To do:
# 1. Compare this set with normal distributions.
# 2. Compare rolls of Josh, Nick, and Mark. Do we all roll normal distributions?
# 3. If I recoreded red and yellow die distinctly, I could then figure out which (if any) account for anomolies.

# Catan Seafares Scenario 1. 1/27/17, 3:45 PM - 5:30 PM. Josh, Nick (winner), Mark (that turn order). 
Game1 = [5,10,7,11,8,8,7,8,7,3,9,8,8,8,6,6,6,4,9,2,4,4,10,8,8,6,3,9,11,4,10,9,6,2,3,10,10,9,10,6,9,9,11,6,6,8,6,10,7,2,6,8,6,4,8,4,7,5,6,8,11,7,8,5,6,6,8,7,6,8,6,9,9,11,9,9,4,6,11,7,7,12,9,8,9,4,6,7,7,10,6,7,7]

# Catan Seafarers Scenario 3. 1/12/17, 8:30 PM - XX:XX PM. Josh, Nick (winner), Mom (that turn order).
Game2 = [4,3,8,9,4,10,6,5,7,6,9,3,5,10,7,6,7,12,2,11,8,10,4,7,8,6,9,7,4,5,8,12,5,5,9,5,5,6,10,3,10,9,8,5,6,7,7,7,10,5,8,5,10,5,7,6,7,2,12,6,7,2,10,4,4,6,5,11,9,8,7,6,9,11,3,9,7,4,2,9,8,7,6,6,6,7,9,9,7,9,7,11,9,8,6,8,5,7,7,8,6,10,5,9,7,5,9,3,7,7,10,7,8,7,12,6,8,9,10,7,9,10,6,8,8,6]

# Other rolls
#Game3 = [6,5,9,7,5,9,4,4,7]

# Homemade random scenario. 1/29/17, 2:30 PM - 4:00 PM. Mom, Nick, Josh (winner) (that turn order).
Game3 = [10,6,8,9,5,4,7,4,6,7,8,4,3,9,4,7,10,4,5,6,5,5,6,7,5,4,6,10,8,3,3,4,6,4,7,11,7,7,6,5,3,8,8,3,4,8,3,9,5,12,7,7,9,3,6,9,9,12,9,11,5,9,7,6,6,7,9,7,7,7,10,7,11,8,10,8,8,7,7,8,9,7,8,12,6,8,5,8]

# Homemade random scenario. 1/29/17, 4:30 PM - ?:?? PM. Nick (winner), Josh, Mom (that turn order).
Game4 = [5,10,8,6,5,8,6,7,9,7,11,10,7,12,10,7,8,8,6,7,9,3,2,5,5,7,9,8,5,6,6,7,9,5,5,6,6,7,9,7,10,10,8,8,6,6,12,6,5,9,5,6,6,11,8,4,10,9,6,8,8,7,5,11,4,12,5,10,6,5,7,6,9,4,7,7,6,7,6,8,6,5,6,7,6,11,6,5,2,8,6,10,7,7,8,4,7,7,7,5,7,9,2,6,7,7,8,7,6,7,7,8,11,10,6,5,4,7,8,5,3,12,6,7,8,8,4,6,7,10,7,10,8,6,6,9,3,6,2,6,7,4,6,10,9,7,7,7,5,6,6,8,8,7,6,5,11,9,9,6,8,5,10,7,9,9,10,9,4,2,7,7]

# Catan. 2/17/19, 7:45 PM - 10:10 PM. Mom, Josh, Emilie (winner) (that turn order).
Game5 = [8,6,11,11,7,8,11,4,8,9,12,7,3,7,9,5,11,8,8,5,9,3,5,6,6,5,3,7,6,7,12,11,11,8,4,9,12,8,4,9,6,7,3,8,2,5,6,11,12,10,5,11,5,6,7,7,7,7,2,12,12,3,11,8,8,4,3,6,2,10,7,8,6,8,6,10,4,4,7,8,4,11,4,3,4,6,7,5]

# 3/2/17, 9:00 PM - ??:??. Josh, Emilie (winner), Mom (that turn order). Mom quit at 0.
Game6 = [8,7,9,9,8,7,4,6,4,12,7,8,7,8,9,10,7,6,9,5,7,6,4,5,4,5,4,9,9,5,8,5,4,5,9,8,9,12,7,8,9,9,8,8,10,9,3,10,10,7,10,4,11,7,7,8,9,6,8,7,9,9,3,7,9,5,10,3,9,2,5,9,9,5,10,7,4,7,0,8,6,12,8,7,4,6,8]

# 3/25/17, 9:15 PM - 10:30 PM. Josh (winner), Nick, Monica (that turn order).
Game7 = [8,8,5,12,4,7,6,11,6,7,6,8,6,8,9,8,5,6,11,8,6,8,7,9,11,5,4,11,3,8,7,11,8,5,8,7,12,11,7,7,7,5,4,9,4,7,8,9,7,8,8,8,4,2,9,9,3,9,8,6,9,8,8]

'''

GameN = [Game1, Game2, Game3, Game4, Game5, Game6, Game7]
N = len(GameN)

#for N in range(1,N+1):
#  for Game in GameN[0]:
#    for i in Game:
for Game in GameN: 
  for roll in Game:

    hGame = ROOT.TH1D('hGame', 'hGame', 14, 0, 14)
    hGame.Fill(roll)

    hGame.Scale(1./len(Game)) # Scale histogram by sample size.
    hGame.GetXaxis().SetNdivisions(221)
    hGame.SetFillColor(N)
    hGame.SetLineColor(N)
    hGame.SetLineWidth(2)
    hGame.SetFillStyle(3003)
    hGame.SetStats(0)

    hGame.SaveAs('Game'+N+'.root')

'''



# Fill histogram with Game1 array
hGame1 = ROOT.TH1D('hGame1', 'hGame1', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Game1:
  hGame1.Fill(i)
# Scale histogram by sample size.
hGame1.Scale(1./len(Game1))
hGame1.GetXaxis().SetNdivisions(221)
hGame1.SetFillColor(ROOT.kMagenta)
hGame1.SetLineColor(ROOT.kMagenta)
hGame1.SetLineWidth(2)
hGame1.SetFillStyle(3003)
hGame1.SetStats(0)

# Fill histogram with Game2 array
hGame2 = ROOT.TH1D('hGame2', 'hGame2', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Game2:
  hGame2.Fill(i)
# Scale histogram by sample size.
hGame2.Scale(1./len(Game2))
hGame2.GetXaxis().SetNdivisions(221)
hGame2.SetFillColor(ROOT.kCyan)
hGame2.SetLineColor(ROOT.kCyan)
hGame2.SetLineWidth(2)
hGame2.SetFillStyle(3003)
hGame2.SetStats(0)

# Fill histogram with Game3 array
hGame3 = ROOT.TH1D('hGame3', 'hGame3', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Game3:
  hGame3.Fill(i)
# Scale histogram by sample size.
hGame3.Scale(1./len(Game3))
hGame3.GetXaxis().SetNdivisions(221)
hGame3.SetFillColor(ROOT.kGreen)
hGame3.SetLineColor(ROOT.kGreen)
hGame3.SetLineWidth(2)
hGame3.SetFillStyle(3003)
hGame3.SetStats(0)

# Fill histogram with Game4 array
hGame4 = ROOT.TH1D('hGame4', 'hGame4', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Game4:
  hGame4.Fill(i)
# Scale histogram by sample size.
hGame4.Scale(1./len(Game4))
hGame4.GetXaxis().SetNdivisions(221)
hGame4.SetFillColor(ROOT.kBlue)
hGame4.SetLineColor(ROOT.kBlue)
hGame4.SetLineWidth(2)
hGame4.SetFillStyle(3003)
hGame4.SetStats(0)

# Fill histogram with Game5 array
hGame5 = ROOT.TH1D('hGame5', 'hGame5', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Game5:
  hGame5.Fill(i)
# Scale histogram by sample size.
hGame5.Scale(1./len(Game5))
hGame5.GetXaxis().SetNdivisions(221)
hGame5.SetFillColor(ROOT.kOrange)
hGame5.SetLineColor(ROOT.kOrange)
hGame5.SetLineWidth(2)
hGame5.SetFillStyle(3003)
hGame5.SetStats(0)

# Fill histogram with Game5 array
hGame6 = ROOT.TH1D('hGame6', 'hGame6', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Game6:
  hGame6.Fill(i)
# Scale histogram by sample size.
hGame6.Scale(1./len(Game6))
hGame6.GetXaxis().SetNdivisions(221)
hGame6.SetFillColor(ROOT.kGray)
hGame6.SetLineColor(ROOT.kGray)
hGame6.SetLineWidth(2)
hGame6.SetFillStyle(3003)
hGame6.SetStats(0)




# Concat all roll sets into single Experimental array
Exp = Game1 + Game2 + Game3 + Game4 + Game5 + Game6
# Fill histogram with Experimental array
hExp = ROOT.TH1D('hExp', 'hExp', 14, 0, 14)#max(Exp)+2, min(Exp), max(Exp))
for i in Exp:
  hExp.Fill(i)
#hExp = hGame1 + hGame2
# Scale histogram by sample size.
hExp.Scale(1./len(Exp))
hExp.GetXaxis().SetNdivisions(221)
hExp.SetFillColor(ROOT.kRed)
hExp.SetLineColor(ROOT.kRed)
hExp.SetLineWidth(3)
hExp.SetFillStyle(3003)
hExp.SetStats(0)


#http://stackoverflow.com/questions/33069476/simulating-rolling-2-dice-in-python
def dice(n):
    return [randint(1, 6) + randint(1, 6) for _ in range(n)]
nd = int(1e6)
Theory = dice(nd)

# Fill histogram with Theoretical array
hTheory = ROOT.TH1D('hTheory', 'hTheory', 14, 0, 14)#max(Theory)+2, min(Theory)-1, max(Theory)+1)
for i in Theory:
  hTheory.Fill(i)
# Scale histogram by sample size.
hTheory.Scale(1./len(Theory))
hTheory.SetFillColor(ROOT.kBlack)#Gray+2)#Black)#ue)
hTheory.SetLineColor(ROOT.kBlack)#Gray+2)#Black)#ue)
hTheory.SetLineWidth(2)
hTheory.SetFillStyle(3003)
hTheory.SetStats(0)


### Plot Exp data in histogram format.
c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetGridy()
hGame4.SetTitle('Characterization of Catan Dice;Sum;Probability')
hGame4.Draw()
hGame2.Draw('same')
hGame3.Draw('same')
hGame1.Draw('same')
hGame5.Draw('same')
hGame6.Draw('same')
hTheory.Draw('same')
hExp.Draw('same')

x1 = 0.75
y1 = 0.65
x2 = 0.89
y2 = 0.85
legend = ROOT.TLegend(x1, y1, x2, y2)
legend.AddEntry(hExp,    'Emperical, '+str('{:,}'.format(len(Exp))), 'f')
legend.AddEntry(hGame1, '    Game 1, '+str('{:,}'.format(len(Game1))), 'f')
legend.AddEntry(hGame2, '    Game 2, '+str('{:,}'.format(len(Game2))), 'f')
legend.AddEntry(hGame3, '    Game 3, '+str('{:,}'.format(len(Game3))), 'f')
legend.AddEntry(hGame4, '    Game 4, '+str('{:,}'.format(len(Game4))), 'f')
legend.AddEntry(hGame5, '    Game 5, '+str('{:,}'.format(len(Game5))), 'f')
legend.AddEntry(hGame6, '    Game 6, '+str('{:,}'.format(len(Game6))), 'f')
legend.AddEntry(hTheory, 'Theoretical, '+str(int(nd/1e6))+'e6', 'f')
legend.SetFillColor(0)
legend.Draw()

c.SaveAs('CatanDiceRecord.pdf')
c.SaveAs('CatanDiceRecord.png')

