### Catan Dice Roll Distribution
I logged player dice rolls during several [Settlers of Catan](https://www.catan.com/) games. I most likely missed (more than) a few rolls here.
I understand that it is probably impossible to produce significant statistics to make any accruate claim.

TODO:
1. ~~Compare this set with normal distributions.~~
2. Compare rolls of Josh, Nick, and Mark, e.g.. Do we all roll normal distributions?
3. If I recoreded red and yellow die distinctly, I could then figure out which (if any) di(c)e account(s) for anomolies.

I saved the real data and theoretical as root files to easily import the data without the need to build histograms for every run.
These files are checked for existance before histogram creation (as would be the case for a new game).
The uncertainty on the averaged rolls (solid black) is shown incorrectly. It should be, at first order, the difference between
the maximum and minimum measurements for each dice sum. However, it was kept as the default counting uncertainty because the
uncertainties of this experiment are so large in general that it really doesn't matter what I do. Plus it's whatever: I just wanted
to make some plots while I played a game.

![Histograms](CatanDiceRecord.png)

![Fits](Gaussian.png)


Note: I wrote this code in ROOT 5. I returned to this code after migrating to a new machine with ROOT 6 installed.
Returning to this (and all (Py)ROOT) code has illuminated several differences in code interpretation between versions.
I've editted things where I felt was necessary. Otherwise, lack of priority prevented me from proper maintenance.
I will leave the current results as qualitative and disown unfinished TODOs.
